﻿using UnityEngine;
using System.Collections;

public class OndulationFollower : MonoBehaviour {

    public GameObject[] ondulations;
    const string START_ONDU = "StartOndulation";

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        UpdateOndulation();

	}

    public void UpdateOndulation()
    {
        for (int cnt = 0; cnt < ondulations.Length; cnt++)
        {
            ondulations[cnt].transform.position = new Vector3(transform.position.x, ondulations[cnt].transform.position.y, ondulations[cnt].transform.position.z);
            Animator anim = ondulations[cnt].GetComponent<Animator>();
            anim.SetTrigger(START_ONDU);
        }

    }
}
