﻿using UnityEngine;
using System.Collections;

public class BackgroundHider : MonoBehaviour {

    public GameObject TitleHider;
    public GameObject[] Clouds;

    const string REVEAL_TITLE = "RevealTitle";
    const string CLOUD_MOVER = "CloudMover";
	
    void Start()
    {
        ShowTitle();
        ShowClouds();
    }

    public void ShowTitle()
    {
        Animator anim = TitleHider.GetComponent<Animator>();
        anim.SetTrigger(REVEAL_TITLE);
    }

    public void ShowClouds()
    {
        for (int cnt = 0;cnt<Clouds.Length;cnt++)
        {
            Animator anim = Clouds[cnt].GetComponent<Animator>();
            anim.SetTrigger(CLOUD_MOVER);
        }
        
    }
}
