﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SceneLoader : MonoBehaviour {

    public Image Fader;
    public float FadeDuration;

    void Start()
    {
        Fader.color = new Color(Fader.color.r, Fader.color.g, Fader.color.b, 1f);
        Fader.CrossFadeAlpha(0f, FadeDuration, true);
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(TransitionToScene(sceneName));
    }

    private IEnumerator TransitionToScene(string sceneName)
    {
        Fader.CrossFadeAlpha(1f, FadeDuration, true);
        yield return new WaitForSeconds(FadeDuration);
        LoadingScreenManager.LaunchScene(sceneName);
        yield return null;
    }

}
