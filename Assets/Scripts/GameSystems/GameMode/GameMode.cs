﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class GameMode : IClonable<GameMode> {

    [SerializeField] private string _name;
    [SerializeField] private string _description;
    [SerializeField] private string _comment;
    [SerializeField] private List<ColorElement> _playerColors;
    [SerializeField] private int _subBoardPerLine;
    [SerializeField] private int _requiredAlignment;
    [SerializeField] private int _minPlayer;

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public string Description
    {
        get { return _description; }
        set { _description = value; }
    }

    public string Comment
    {
        get { return _comment; }
        set { _comment = value; }
    }

    public ColorElement this[int index]
    {
        get { return _playerColors[index]; }
        set { _playerColors[index] = value; }
    }

    public int SubBoardPerLine
    {
        get { return _subBoardPerLine; }
        set { _subBoardPerLine = value; }
    }

    public int RequiredAlignement
    {
        get { return _requiredAlignment; }
        set { _requiredAlignment = value; }
    }

    public int MinPlayer
    {
        get { return _minPlayer; }
        set { _minPlayer = value; }
    }

    public int MaxPlayer
    {
        get { return _playerColors.Count; }
    }

    public int SubBoardLength
    {
        get { return 3; }
    }

    
    public int TotalSubBoard
    {
        get { return (int) Mathf.Pow(_subBoardPerLine,2f); }
    }

    public int TotalBoardLength
    {
        get { return SubBoardLength * SubBoardPerLine; }
    }

    public GameMode()
    {
        _name = "";
        _description = "";
        _comment = "";
        _playerColors = new List<ColorElement>();
        _subBoardPerLine = 0;
        _requiredAlignment = 0;
        _minPlayer = 1;
    }

    public GameMode(string name)
    {
        _name = name;
        _description = "";
        _comment = "";
        _playerColors = new List<ColorElement>();
        _subBoardPerLine = 0;
        _requiredAlignment = 0;
        _minPlayer = 1;
    }

    public void AddColor(ColorElement color)
    {
        if (!_playerColors.Contains(color))
        {
            _playerColors.Add(color);
            UpdateOrder();
        }
           
    }

    public void RemoveColor(ColorElement color)
    {
        if (_playerColors.Contains(color))
        {
            _playerColors.Remove(color);
            UpdateOrder();
        }
            
    }

    public void RemoveAll()
    {
        _playerColors.Clear();
    }

    public int CountColor()
    {
        return _playerColors.Count;
    }

    public void Move(int from,int to)
    {
        ColorElement temp = CloneColor(_playerColors[to]);
        _playerColors[to] = CloneColor(_playerColors[from]);
        _playerColors[from] = temp;
        UpdateOrder();
    }

    public void SetColors(ColorElement[] c)
    {
        for (int i = 0; i< c.Length;i++)
        {
            _playerColors[i] = c[i];
        }
    }

    public ColorElement CloneColor(ColorElement c)
    {
        ColorElement clone = new ColorElement();
        clone.Color = c.Color;
        clone.Name = c.Name;
        return clone;
    }

    public ColorElement[] CloneColor()
    {
        ColorElement[] c = new ColorElement[_playerColors.Count];
        _playerColors.CopyTo(c);
        return c;
    }

    public GameMode Clone()
    {
        GameMode gm = new GameMode();
        gm.Name = _name;
        ColorElement[] c = new ColorElement[_playerColors.Count];
        gm.SetColors(c);
        gm.SubBoardPerLine = _subBoardPerLine;
        return gm;

    }

    private void UpdateOrder()
    {
        for (int cntColor = 0; cntColor < _playerColors.Count; cntColor++)
        {
            _playerColors[cntColor].Order = cntColor;
        }
    }
}
