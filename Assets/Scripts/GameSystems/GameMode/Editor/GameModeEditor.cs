﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameModeDatabase))]
public partial class GameModeEditor : BaseEditor<GameModeDatabase,GameMode> {
    
    protected override void ViewRecordData(int index)
    {
        ViewGameMode(index);

        EditorGUILayout.BeginVertical();

        EditorGUILayout.BeginHorizontal();

        ViewBoardModelizer(index);
        ViewBoardLayout(index);
        ViewOtherInfo(index);

        EditorGUILayout.EndHorizontal();
        
        ViewGameRule(index);

        EditorGUILayout.EndVertical();

        GUILayout.Space(SPACE_SIZE);
        
    }

}
