﻿using UnityEngine;
using UnityEditor;

public partial class CharacterEditor {

    const float SPACE_SIZE = 10f;

    private void ViewCharacter(int index)
    {
        EditorGUILayout.BeginHorizontal();

        ViewCharacterData(index);

        ViewPickers(index);
        
        EditorGUILayout.EndHorizontal();

        CheckPickersEvent(index);

    }

    #region Private Methods

    private void InitIds()
    {
        avatarIds = new int[myDb.Count];
        portraitIds = new int[myDb.Count];

        for (int cntId = 0; cntId < myDb.Count ; cntId++)
        {
            avatarIds[cntId] = 0;
            portraitIds[cntId] = 0;
        }

    }

    private void ShowAvatarPicker(int index)
    {

        avatarIds[index] = GUIUtility.GetControlID(FocusType.Passive) + 100 + index;
        EditorGUIUtility.ShowObjectPicker<Sprite>(null, true, "Avatar_", avatarIds[index]);
    }

    private void ShowPortraitPicker(int index)
    {

        portraitIds[index] = GUIUtility.GetControlID(FocusType.Passive) + 100 + index;
        EditorGUIUtility.ShowObjectPicker<Sprite>(null, true, "Portrait_", portraitIds[index]);
    }

    private void ViewPickers(int index)
    {
        EditorGUILayout.BeginVertical();
        if (myDb[index].Avatar)
            DefaultAvatar = myDb[index].Avatar.texture;
        else
            DefaultAvatar = null;

        if (myDb[index].Portrait)
            DefaultPortrait = myDb[index].Portrait.texture;
        else
            DefaultPortrait = null;

        GUILayout.Label("Portrait :", _textStyle);
        if (GUILayout.Button(new GUIContent(DefaultPortrait, "Select"), GUILayout.Width(150f), GUILayout.Height(150f)))
        {
            InitIds();
            ShowPortraitPicker(index);
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical();

        GUILayout.Label("Avatar :", _textStyle);

        if (GUILayout.Button(new GUIContent(DefaultAvatar, "Select"), GUILayout.Width(150f), GUILayout.Height(150f)))
        {
            InitIds();
            ShowAvatarPicker(index);
        }


        EditorGUILayout.EndVertical();
    }

    private void ViewCharacterData(int index)
    {
        EditorGUILayout.BeginVertical();

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Name : ", _textStyle, GUILayout.Width(70f));

        myDb[index].Name = GUILayout.TextField(myDb[index].Name, _textfieldStyle, GUILayout.Width(150f));

        EditorGUILayout.EndHorizontal();

        GUILayout.Space(5 * SPACE_SIZE);

        EditorGUILayout.LabelField("Character Bio : ", _textStyle, GUILayout.ExpandWidth(false));

        myDb[index].Bio = EditorGUILayout.TextArea(myDb[index].Bio, _textareaStyle, GUILayout.Height(75f));

        EditorGUILayout.EndVertical();
    }

    private void CheckPickersEvent(int index)
    {
        string commandName = Event.current.commandName;

        if (commandName == "ObjectSelectorUpdated")
        {
            if (EditorGUIUtility.GetObjectPickerControlID() == portraitIds[index])
                myDb[index].Portrait = (Sprite)EditorGUIUtility.GetObjectPickerObject();
            else if (EditorGUIUtility.GetObjectPickerControlID() == avatarIds[index])
                myDb[index].Avatar = (Sprite)EditorGUIUtility.GetObjectPickerObject();

            Repaint();
        }

        if (commandName == "ObjectSelectorClosed")
        {
            if (EditorGUIUtility.GetObjectPickerControlID() == portraitIds[index])
                portraitIds[index] = 0;
            else if (EditorGUIUtility.GetObjectPickerControlID() == avatarIds[index])
                avatarIds[index] = 0;

        }
    }

    #endregion

}
