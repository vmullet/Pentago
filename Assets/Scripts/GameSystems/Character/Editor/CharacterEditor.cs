﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CharacterDatabase))]
public partial class CharacterEditor : BaseEditor<CharacterDatabase,Character> {

    private Texture2D DefaultAvatar;
    private Texture2D DefaultPortrait;
    private int[] avatarIds;
    private int[] portraitIds;

    protected override void ViewRecordData(int index)
    {
        ViewCharacter(index);
    }

}
