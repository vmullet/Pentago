﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    #region Variables

    /* Game Structure */
    public GameMode SelectedGameMode;
    public GameObject SelectedSubBoard;

    public Dictionary<string, GameObject> FullBoard;
    public ColorElement ActualColor;
    public List<Player> Players;
    public HashSet<string> FreePositions;
    public List<GameObject> SubBoards;

    /* Board Structure */

    public int BoardLength
    {
        get { return SelectedGameMode.TotalBoardLength; }
    }
    public int SubBoardLength
    {
        get { return SelectedGameMode.SubBoardLength; }
    }
    public int SubBoardPerLine
    {
        get { return SelectedGameMode.SubBoardPerLine; }
    }
    public int TotalSubBoard
    {
        get { return SelectedGameMode.TotalSubBoard; }
    }

    #endregion

    void Awake()
    {
        if (Instance != null && Instance !=this)
        {
            Destroy(gameObject);
        }
            
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
       
    }

	// Use this for initialization
	void Start () {

        FullBoard = new Dictionary<string, GameObject>();
        Players = new List<Player>();
        FreePositions = new HashSet<string>();
        SubBoards = new List<GameObject>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    #region DebugMethods

    public void ShowBoard()
    {
        string theBoard = "";
        for (int i = 0; i<BoardLength;i++)
        {
            for (int j = 0; j < BoardLength; j++)
            {
                string coord = (char)(i+97) + "" + j;
                theBoard += FullBoard[coord].name+" ";
            }
            theBoard += "\n";
        }
        Debug.Log(theBoard);
    }

    public void ShowPlayers()
    {
        for (int cntPlayer = 0;cntPlayer < Players.Count; cntPlayer++)
        {
            Debug.Log(Players[cntPlayer].Character.Name + " " + Players[cntPlayer].IsBot);
        }
    }

    #endregion

    public string CheckWinLine(string mode)
    {
        string win = "";
        int nbAlign = 1;
        for (int cntLine = 0; cntLine < BoardLength; cntLine++)
        {
            for (int cntColumn = 0; cntColumn < BoardLength - 1; cntColumn++)
            {
                string coordinate = (mode == "horiz" ? (char)(cntLine + 97) + "" + cntColumn : (char)(cntColumn + 97) + "" + cntLine);
                string nextcoordinate = (mode == "horiz" ? (char)(cntLine + 97) + "" + (cntColumn + 1) : (char)(cntColumn + 1 + 97) + "" + cntLine);
                if (FullBoard[coordinate].GetComponent<CellInformation>().Content != "empty")
                    if (FullBoard[coordinate].GetComponent<CellInformation>().Content ==
                        FullBoard[nextcoordinate].GetComponent<CellInformation>().Content)
                    {
                        nbAlign++;
                        win += coordinate + ";";
                    }
                    else
                    {
                        nbAlign = 1;
                        win = "";
                    }
                if (nbAlign == SelectedGameMode.RequiredAlignement)
                {
                    win += nextcoordinate;
                    return win;
                }
            }
            nbAlign = 1;
            win = "";
        }

        return win;
    }

    public string[][] GetDiagonal(bool toptoBottom)
    {

        int offset = SelectedGameMode.RequiredAlignement - 1;

        string[][] diagonals = new string[BoardLength * 2 - 1 - 2 * offset][];
        for (int cntDiagonal = offset; cntDiagonal < BoardLength * 2 - 1 - offset; cntDiagonal++)
        {
            int diagSize = (cntDiagonal < BoardLength ? cntDiagonal + 1 : BoardLength * 2 - (cntDiagonal + 1));

            diagonals[cntDiagonal - offset] = new string[diagSize];
            int diagLine = (cntDiagonal < BoardLength ? 0 : cntDiagonal - BoardLength + 1);
            int diagColumn = (cntDiagonal < BoardLength ? cntDiagonal : BoardLength - 1);

            if (!toptoBottom)
                diagLine = (cntDiagonal < BoardLength ?
                            BoardLength - 1 : 2 * (BoardLength - 1) - cntDiagonal);

            for (int diagCell = 0; diagCell < diagSize; diagCell++)
            {
                diagonals[cntDiagonal - offset][diagCell] = (char)(diagLine + 97) + "" + diagColumn;
                diagLine = (toptoBottom ? diagLine + 1 : diagLine - 1);
                diagColumn--;

            }

        }

        return diagonals;
    }

    public string CheckWinDiag(bool toptoBottom)
    {
        string winCoord = "";
        int nbAlign = 1;
        string[][] diag = GetDiagonal(toptoBottom);

        for (int i = 0; i < diag.Length; i++)
        {
            for (int j = 0; j < diag[i].Length - 1; j++)
            {
                string coordinate = diag[i][j];
                string nextcoordinate = diag[i][j + 1];
                if (FullBoard[coordinate].GetComponent<CellInformation>().Content != "empty")
                    if (FullBoard[coordinate].GetComponent<CellInformation>().Content ==
                        FullBoard[nextcoordinate].GetComponent<CellInformation>().Content)
                    {
                        nbAlign++;
                        winCoord += coordinate + ";";
                    }
                    else
                    {
                        nbAlign = 1;
                        winCoord = "";
                    }

                if (nbAlign == SelectedGameMode.RequiredAlignement)
                {
                    winCoord += nextcoordinate;
                    return winCoord;
                }
            }

            nbAlign = 1;
            winCoord = "";

        }

        return winCoord;

    }

    
}
