﻿using UnityEngine;
using UnityEditor;

public abstract partial class BaseEditor<T,U> : Editor where T : Database<U> where U : IClonable<U> {

    private GUIStyle globalStyle;
    private GUIStyle subStyle;
    private bool isRemoved;

    protected T myDb;

    void Awake()
    {
        if (myDb == null)
        {
            myDb = (T) target;
        }
    }

    public override void OnInspectorGUI()
    {

        PrepareSkins();
        PrepareStyles();

        EditorGUILayout.BeginVertical(globalStyle);

        Title("Paletto " + typeof(U).ToString(), "Editor", "v1.0");
        Header();
        HUD();

        for (int cntRecord = 0; cntRecord < myDb.Count; cntRecord++)
        {
            isRemoved = false;
            EditorGUILayout.BeginVertical(subStyle);

            RemoveArea(cntRecord);
            if (!isRemoved)
            {
                ViewRecordData(cntRecord);
            }
            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.EndVertical();
    }

    protected void PrepareSkins()
    {
        globalStyle = new GUIStyle();
        globalStyle.normal.background = GUIStyleManager.TextureMaker(2, 2, new Color(0, 0, 0, .75f));
        subStyle = new GUIStyle(GUI.skin.box);
        subStyle.normal.background = GUIStyleManager.TextureMaker(2, 2, new Color(.7f, .7f, .7f));
    }

    protected abstract void ViewRecordData(int index);

}