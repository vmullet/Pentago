﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnnouncementPanel : MonoBehaviour {

    private GameManager _gm;
    private Animator _anim;
    private LetterByLetter _lbl;
    private Text _lblText;
    private bool _endScroll;

    public GameObject _announcementText;
    public GameObject PlayerList;
    public GameObject PlayerElement;
    public Image Fader;
    public RadialMenu _radialMenu;
    public GameObject BoardContainer;
    public GameObject TurnList;
    public PalettoStateMachine StateMachine;
    

    void Awake()
    {
        _gm = GameManager.Instance;
        _anim = GetComponent<Animator>();
        _lbl = _announcementText.GetComponent<LetterByLetter>();
        _lblText = _lbl.GetComponent<Text>();
    }

	// Use this for initialization
	void Start () {
        _endScroll = false;
        StartCoroutine(Announce());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetEndScroll()
    {
        _endScroll = true;
    }

    private IEnumerator Announce()
    {
        yield return new WaitForSeconds(1f);

        _anim.SetBool("Scroll", true);

        yield return new WaitUntil(() => _endScroll  );

        yield return StartCoroutine(FadeUI(Fader,.75f, .25f));

        yield return StartCoroutine(_lbl.DisplayLByL("bonjour a vous , jeunes guerriers venus de tout horizons. la roue du destin va maintenant vous attribuer une couleur"));

        BuildAvatarList();

        yield return StartCoroutine(FadeUI(_lblText,0f, .25f));

        yield return StartCoroutine(_radialMenu.ColorAffector());

        yield return StartCoroutine(_lbl.DisplayLByL("bien, il est maintenant temps de determiner l'ordre de jeu"));

        yield return StartCoroutine(FadeUI(_lblText,0f, .25f));

        yield return StartCoroutine(ReorderPlayers());

        yield return StartCoroutine(_lbl.DisplayLByL("voila qui est mieux , l'ordre des tours definitifs est donc ..."));

        yield return StartCoroutine(FadeUI(_lblText, 0f, .25f));

        yield return StartCoroutine(DestroyAvatarList());

        _anim.SetBool("Scroll", false);

        yield return new WaitForSeconds(1f);

        yield return StartCoroutine(FadeUI(transform.Find("Background").GetComponent<Image>(), 0f, .25f));

        yield return StartCoroutine(FadeUI(transform.Find("RightScroll").GetComponent<Image>(), 0f, .25f));

        yield return StartCoroutine(FadeUI(Fader, 0f, .25f));

        //Build the GameBoard and Start the StateMachine
        yield return StartCoroutine(PrepareGame());

        StateMachine.enabled = true;

        //End the cutscene
        gameObject.SetActive(false);

    }

    private void BuildAvatarList()
    {
        _radialMenu.PlayerAvatars = new GameObject[_gm.Players.Count];
        for (int cntPlayer = 0; cntPlayer < _gm.Players.Count; cntPlayer++)
        {
            GameObject playerElement = Instantiate(PlayerElement, PlayerList.transform, false) as GameObject;
            playerElement.transform.GetChild(1).GetComponent<Image>().sprite = _gm.Players[cntPlayer].Character.Avatar;
            playerElement.name = "Player_" + cntPlayer;
            _radialMenu.PlayerAvatars[cntPlayer] = playerElement;
            Animator anim = playerElement.GetComponent<Animator>();
            anim.SetBool("Show", true);
        }
    }

    private IEnumerator DestroyAvatarList()
    {
        for (int cntPlayer = 0; cntPlayer < _gm.Players.Count; cntPlayer++)
        {
            Animator anim = PlayerList.transform.GetChild(cntPlayer).GetComponent<Animator>();
            anim.SetTrigger("Unfield");
            Image img = PlayerList.transform.GetChild(cntPlayer).GetChild(0).GetComponent<Image>();
            img.CrossFadeAlpha(0f, .5f, true);
        }

        yield return new WaitForSeconds(.5f);

        for (int cntPlayer = 0; cntPlayer < _gm.Players.Count; cntPlayer++)
        {
            Destroy(PlayerList.transform.GetChild(cntPlayer).gameObject);
        }

    }

    private IEnumerator FadeUI(Graphic gph,float alpha,float duration)
    {
        yield return new WaitForSeconds(.1f);

        gph.CrossFadeAlpha(alpha, duration, true);

        yield return new WaitForSeconds(duration);

        if (alpha==0)
        gph.enabled = false;
        else
        gph.enabled = true;

        yield return null;

    }

    private IEnumerator ReorderPlayers()
    {

        int nbPlayer = PlayerList.transform.childCount;

        for (int cntPlayer = 0; cntPlayer < nbPlayer; cntPlayer++)
        {
            ColorData actualPlayer = PlayerList.transform.GetChild(cntPlayer).GetComponent<ColorData>();
            StartCoroutine(actualPlayer.MoveToCorrectPlace()); 
        }

        yield return new WaitUntil(() => PlayerList.transform.GetChild(nbPlayer - 1).GetComponent<ColorData>().isFinished);

    }

    private IEnumerator PrepareGame()
    {
        BoardContainer.SetActive(true);
        BoardContainer.transform.Find("GameBoard").GetComponent<GameBoard>().StartTheGame();
        Animator anim = BoardContainer.GetComponent<Animator>();
        anim.SetTrigger("Move");

        TurnList.SetActive(true);
        TurnList.GetComponent<Animator>().SetTrigger("Move");
        yield return new WaitForSeconds(2f);

    }

}
