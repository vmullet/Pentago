﻿using UnityEngine;
using System.Collections.Generic;

public abstract class Database<U> : ScriptableObject where U : IClonable<U> {

    [SerializeField] protected List<U> _database;

    public U this[int index]
    {
        get { return _database[index]; }
    }

    public int Count
    {
        get { return _database.Count; }
    }

    public void Add(U record)
    {
        _database.Add(record);
    }

    public void Remove(U record)
    {
        _database.Remove(record);
    }

    public void RemoveAt(int index)
    {
        _database.RemoveAt(index);
    }

    public void RemoveAll()
    {
        _database.Clear();
    }

    public void Move(int from, int to)
    {
        U temp = _database[to].Clone();
        _database[to] = _database[from];
        _database[from] = temp;
    }

    public Database()
    {
        _database = new List<U>();
    }


}
