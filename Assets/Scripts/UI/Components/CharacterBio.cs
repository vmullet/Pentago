﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterBio : MonoBehaviour {

    private Text _nameField;
    private Text _bioField;
    private Image _avatarImg;
    private Image _portraitImg;
    private RectTransform _rct;

    public GameObject ContentPanel; 

    void Awake()
    {
        _rct = GetComponent<RectTransform>();
        _nameField = transform.Find("Name").GetComponent<Text>();
        _bioField = transform.Find("Bio").GetComponent<Text>();
        _avatarImg = transform.Find("Avatar").GetComponent<Image>();
        _portraitImg = transform.Find("Portrait").GetComponent<Image>();
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ShowBio(Character character)
    {
        gameObject.SetActive(true);
        _rct.localPosition = Vector3.zero;
        _nameField.text = character.Name;
        _bioField.text = character.Bio;
        _avatarImg.sprite = character.Avatar;
        _portraitImg.sprite = character.Portrait;
    }

    public void CloseBio()
    {
        ContentPanel.GetComponent<Animator>().SetBool("Fade", false);
        gameObject.SetActive(false);
    }

    
}
