﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterChanger : MonoBehaviour {

    private int _characterIndex;
    private CharacterDatabase _cdb;
    private PlayerData _pdata;
    private Image _avatar;
    
    void Awake()
    {
       _cdb = GameManager.Instance.GetComponent<DataBaseLinker>().CharacterDataBase;
       _pdata = transform.parent.GetComponent<PlayerData>();
       _avatar = transform.Find("PlayerAvatar").GetComponent<Image>();
    }

	// Use this for initialization
	void Start () {
        _characterIndex = 0;
        _avatar.sprite = _cdb[_characterIndex].Avatar;
        _pdata.Player.Character = _cdb[_characterIndex];
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ChangeCharacter(int add)
    {
        _characterIndex += add;
        if (_characterIndex > _cdb.Count - 1)
            _characterIndex = 0;

        if (_characterIndex < 0)
            _characterIndex = _cdb.Count - 1;

        transform.Find("PlayerAvatar").GetComponent<Image>().sprite = _cdb[_characterIndex].Avatar;
        _pdata.SetCharacter(_cdb[_characterIndex]);
    }

    
}
