﻿using UnityEngine;
using UnityEngine.UI;

public class GameModeList : MonoBehaviour {

    public GameModeDatabase Database;
    public GameObject GameModePrefab;
    public GameObject DescriptionPanel;
    public GameObject FooterPanel;
    public GameObject ColorPanel;
    public Text TurnOrderLabel;
    public Text DescriptionLabel;
    public Text SelectedModeLabel;
    public Text PlayerNumberLabel;
    public GameObject SelectedGameMode;
    public NumberOfPlayers NumberOfPlayers;

	// Use this for initialization
	void Start () {
        BuildList();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void BuildList()
    {
        for (int cntMode = 0; cntMode < Database.Count;cntMode++)
        {
            GameObject gameModeObject = Instantiate(GameModePrefab);
            gameModeObject.transform.SetParent(transform, false);
            gameModeObject.name = "Mode_"+ Database[cntMode].Name;
            GameModeData MyData = gameModeObject.GetComponent<GameModeData>();
            MyData.MyGameMode = Database[cntMode];
            MyData.MyParent = this;
            gameModeObject.transform.Find("ModeName").GetComponent<Text>().text = Database[cntMode].Name;
        }
    }
}
