﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class RadialMenu : MonoBehaviour {

    private GameManager _gm;

    public GameObject RadialButton;
    public GameObject[] PlayerAvatars;
    public int MaxRotationLoop;


    void Awake()
    {
        _gm = GameManager.Instance;
    }

	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void LaunchRadial()
    {
        StartCoroutine(ColorAffector());
    }

    private void BuildRadialMenu()
    {
        int nbColor = _gm.SelectedGameMode.CountColor();

        for (int cntRdButton = 0; cntRdButton < nbColor;cntRdButton++)
        {
            GameObject theButton = Instantiate(RadialButton, transform, false) as GameObject;
            theButton.GetComponent<Image>().color = _gm.SelectedGameMode[cntRdButton].Color;
            theButton.GetComponent<ColorData>().ColorElement = _gm.SelectedGameMode[cntRdButton].Clone();
            float theta = (Mathf.PI * 2.0f / nbColor)*cntRdButton;
            float posX = Mathf.Sin(theta) * (GetComponent<RectTransform>().rect.width / 2f - theButton.GetComponent<RectTransform>().rect.width / 2f);
            float posY = Mathf.Cos(theta) * (GetComponent<RectTransform>().rect.width / 2f - theButton.GetComponent<RectTransform>().rect.width / 2f);
            theButton.transform.localPosition = new Vector3(posX, posY, theButton.transform.localPosition.z);
        }

    }

    public IEnumerator ColorAffector()
    {
        BuildRadialMenu();

        int totalPlayer = _gm.Players.Count;
        int actualPlayer = 0;
        int actualRotation = MaxRotationLoop;

        System.Random r = new System.Random();

          while(actualPlayer < totalPlayer)
          {
              transform.Rotate(Vector3.forward,actualRotation*Time.deltaTime*1.5f);
              
              if (actualRotation<=MaxRotationLoop-(MaxRotationLoop/(totalPlayer - actualPlayer +1)))
              {
                int randomColor = r.Next(0, transform.childCount);
                Transform selected = transform.GetChild(randomColor);
                float distance = 1f;
                while (distance >.001f)
                {
                    selected.position = Vector3.MoveTowards(selected.position, PlayerAvatars[actualPlayer].transform.position, 100f * Time.deltaTime);
                    yield return new WaitForSeconds(.01f);

                    distance = Vector3.Distance(selected.position, PlayerAvatars[actualPlayer].transform.position);
                    
                }

                GameObject playerColor = PlayerAvatars[actualPlayer].transform.GetChild(0).gameObject;
                
                playerColor.GetComponent<Image>().color = transform.GetChild(randomColor).GetComponent<Image>().color;

                playerColor.transform.parent.GetComponent<ColorData>().ColorElement = transform.GetChild(randomColor).GetComponent<ColorData>().ColorElement;
               
                _gm.Players[actualPlayer].ColorElement = transform.GetChild(randomColor).GetComponent<ColorData>().ColorElement.Clone();
                Destroy(transform.GetChild(randomColor).gameObject);
                yield return new WaitForEndOfFrame();

                actualPlayer++;

              }

              actualRotation -= 3;
              yield return new WaitForEndOfFrame();

          }

        yield return StartCoroutine(DestroyAbandonedChild());

        CorrectColorIndex();

        yield return null;
    }

    private IEnumerator DestroyAbandonedChild()
    {
        if (transform.childCount > 0)
        {
            for (int cntChild = 0; cntChild < transform.childCount; cntChild++)
            {
                transform.GetChild(cntChild).GetComponent<Image>().CrossFadeAlpha(0f, .25f, true);
                yield return new WaitForSeconds(.25f);
                Destroy(transform.GetChild(cntChild).gameObject);
            }
        }
        yield return null;
    }

    private void CorrectColorIndex()
    {
        PlayerAvatars = PlayerAvatars.OrderBy(pa => pa.GetComponent<ColorData>().ColorElement.Order).ToArray();
        _gm.Players = _gm.Players.OrderBy(pl => pl.ColorElement.Order).ToList();

        for (int cntOrder = 0;cntOrder < PlayerAvatars.Length; cntOrder++)
        {
            PlayerAvatars[cntOrder].GetComponent<ColorData>().ColorElement.Order = cntOrder;
            _gm.Players[cntOrder].ColorElement.Order = cntOrder;
        }
    }

    
}
